﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CertificateConverter
{
    public partial class index : Page
    {
        #region CertImport
        protected void Page_Load(object sender, EventArgs e)
        {
            //Clear Errors
            string[] attributes = DisplayError.Attributes["class"].Split(null);
            if (!attributes.Contains("hidden"))
            {
                DisplayError.Attributes.Add("class", string.Join(" ", attributes) + " hidden");
            }
            //Check for Files
            if (IsPostBack)
            {
                if (Cert.PostedFile != null && Cert.PostedFile.FileName.Length > 0)
                {
                    Session.Clear();
                    try
                    {
                        //Open Certificate
                        X509Certificate2 x509 = new X509Certificate2();
                        x509.Import(Cert.FileBytes, OldPassword.Text, X509KeyStorageFlags.Exportable);
                        //Get Names and Passwords
                        CertFile.Text = Cert.FileName;
                        NewPassword.Text = OldPassword.Text;
                        string[] certnamearray = Cert.FileName.Split('.');
                        //Save Certificate to Session
                        Session["Certname"] = certnamearray.Length > 1 ? string.Join(".", certnamearray.Take(certnamearray.Count() - 1)) : Cert.FileName;
                        Session["Certificate"] = x509;
                        SetCertFields();
                    }
                    catch (Exception ex)
                    {
                        ShowError("Error loading certificate", ex.Message);
                        Debug.Print(ex.Message);
                    }
                }
                else if (PrivateKeyFile.PostedFile != null && PrivateKeyFile.PostedFile.FileName.Length > 0)
                {
                    try
                    {
                        //Load Certificate from Session
                        X509Certificate2 x509 = (X509Certificate2)Session["Certificate"];
                        //Add Private Key
                        byte[] keyBuffer = GetBytesFromPEM(new StreamReader(PrivateKeyFile.PostedFile.InputStream).ReadToEnd());
                        RSACryptoServiceProvider prov = DecodeRsaPrivateKey(keyBuffer);
                        x509.PrivateKey = prov;
                        PrivateKey.Checked = x509.HasPrivateKey;
                    }
                    catch (Exception ex)
                    {
                        ShowError("Error loading private key", ex.Message);
                        Debug.Print(ex.Message);
                    }
                }
            }
        }

        private void SetCertFields()
        {
            //Load Certificate from Session
            X509Certificate2 x509 = (X509Certificate2) Session["Certificate"];
            //Set Certificate Information
            R_Subject.Text = x509.Subject;
            R_Issuer.Text = x509.Issuer;
            R_Version.Text = x509.Version.ToString();
            R_ValidDate.Text = x509.NotBefore.ToString();
            R_ExpiryDate.Text = x509.NotAfter.ToString();
            R_Thumbprint.Text = x509.Thumbprint;
            R_SerialNumber.Text = x509.SerialNumber;
            R_PublicKeyFormat.Text = x509.PublicKey.EncodedKeyValue.Format(true);
            R_RawDataLength.Text = x509.RawData.Length.ToString();
            R_CertificateFormat.Text = x509.PublicKey.Oid.FriendlyName;
            //Friendly Name
            FriendlyName.Text = x509.FriendlyName;
            //Private Key
            PrivateKey.Checked = x509.HasPrivateKey;
        }
        #endregion
        #region Export
        void ExportCert(X509ContentType contenttype)
        {
            //Load Session Certificate
            X509Certificate2 x509 = (X509Certificate2) Session["Certificate"];
            //Set Friendly Name
            x509.FriendlyName = FriendlyName.Text;
            //Download Certificate
            byte[] cert = (NewPassword.Text != "" ? x509.Export(contenttype, NewPassword.Text) : x509.Export(contenttype));
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Session["Certname"] + "." + contenttype.ToString());
            Response.AddHeader("Content-Length", cert.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.Flush();
            Response.BinaryWrite(cert);
            Response.End();
        }

        protected void CER_Click(object sender, EventArgs e)
        {
            ExportCert(X509ContentType.Cert);
        }

        protected void P12_Click(object sender, EventArgs e)
        {
            ExportCert(X509ContentType.Pkcs12);
        }

        protected void PFX_Click(object sender, EventArgs e)
        {
            ExportCert(X509ContentType.Pfx);
        }
        #endregion
        #region Functions
        private void ShowError(string headline, string message)
        {
            string[] attributes = DisplayError.Attributes["class"].Split(null);
            DisplayError.Attributes.Remove("class");
            DisplayError.Attributes.Add("class", string.Join(" ", attributes.Where(val => val != "hidden").ToArray()));
            ErrorHeader.InnerText = headline;
            ErrorMessage.InnerText = message;
        }

        //Private Key
        private byte[] GetBytesFromPEM(string pemString)
        {
            string header;
            string footer;

            header = "-----BEGIN RSA PRIVATE KEY-----";
            footer = "-----END RSA PRIVATE KEY-----";

            int start = pemString.IndexOf(header) + header.Length;
            int end = pemString.IndexOf(footer, start) - start;
            return Convert.FromBase64String(pemString.Substring(start, end));
        }

        private static int DecodeIntegerSize(BinaryReader rd)
        {
            byte byteValue;
            int count;

            byteValue = rd.ReadByte();
            if (byteValue != 0x02)        // indicates an ASN.1 integer value follows
                return 0;

            byteValue = rd.ReadByte();
            if (byteValue == 0x81)
            {
                count = rd.ReadByte();    // data size is the following byte
            }
            else if (byteValue == 0x82)
            {
                byte hi = rd.ReadByte();  // data size in next 2 bytes
                byte lo = rd.ReadByte();
                count = BitConverter.ToUInt16(new[] { lo, hi }, 0);
            }
            else
            {
                count = byteValue;        // we already have the data size
            }

            //remove high order zeros in data
            while (rd.ReadByte() == 0x00)
            {
                count -= 1;
            }
            rd.BaseStream.Seek(-1, System.IO.SeekOrigin.Current);

            return count;
        }

        private byte[] AlignBytes(byte[] inputBytes, int alignSize)
        {
            int inputBytesSize = inputBytes.Length;

            if ((alignSize != -1) && (inputBytesSize < alignSize))
            {
                byte[] buf = new byte[alignSize];
                for (int i = 0; i < inputBytesSize; ++i)
                {
                    buf[i + (alignSize - inputBytesSize)] = inputBytes[i];
                }
                return buf;
            }
            else
            {
                return inputBytes;      // Already aligned, or doesn't need alignment
            }
        }

        private RSACryptoServiceProvider DecodeRsaPrivateKey(byte[] privateKeyBytes)
        {
            MemoryStream ms = new MemoryStream(privateKeyBytes);
            BinaryReader rd = new BinaryReader(ms);

            try
            {
                byte byteValue;
                ushort shortValue;

                shortValue = rd.ReadUInt16();

                switch (shortValue)
                {
                    case 0x8130:
                        // If true, data is little endian since the proper logical seq is 0x30 0x81
                        rd.ReadByte(); //advance 1 byte
                        break;
                    case 0x8230:
                        rd.ReadInt16();  //advance 2 bytes
                        break;
                    default:
                        Debug.Assert(false);     // Improper ASN.1 format
                        return null;
                }

                shortValue = rd.ReadUInt16();
                if (shortValue != 0x0102) // (version number)
                {
                    Debug.Assert(false);     // Improper ASN.1 format, unexpected version number
                    return null;
                }

                byteValue = rd.ReadByte();
                if (byteValue != 0x00)
                {
                    Debug.Assert(false);     // Improper ASN.1 format
                    return null;
                }

                // The data following the version will be the ASN.1 data itself, which in our case
                // are a sequence of integers.

                // In order to solve a problem with instancing RSACryptoServiceProvider
                // via default constructor on .net 4.0 this is a hack
                CspParameters parms = new CspParameters();
                parms.Flags = CspProviderFlags.NoFlags;
                parms.KeyContainerName = Guid.NewGuid().ToString().ToUpperInvariant();
                parms.ProviderType = ((Environment.OSVersion.Version.Major > 5) || ((Environment.OSVersion.Version.Major == 5) && (Environment.OSVersion.Version.Minor >= 1))) ? 0x18 : 1;

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(parms);
                RSAParameters rsAparams = new RSAParameters();

                rsAparams.Modulus = rd.ReadBytes(DecodeIntegerSize(rd));

                // Argh, this is a pain.  From emperical testing it appears to be that RSAParameters doesn't like byte buffers that
                // have their leading zeros removed.  The RFC doesn't address this area that I can see, so it's hard to say that this
                // is a bug, but it sure would be helpful if it allowed that. So, there's some extra code here that knows what the
                // sizes of the various components are supposed to be.  Using these sizes we can ensure the buffer sizes are exactly
                // what the RSAParameters expect.  Thanks, Microsoft.
                RSAParameterTraits traits = new RSAParameterTraits(rsAparams.Modulus.Length * 8);

                rsAparams.Modulus = AlignBytes(rsAparams.Modulus, traits.size_Mod);
                rsAparams.Exponent = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_Exp);
                rsAparams.D = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_D);
                rsAparams.P = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_P);
                rsAparams.Q = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_Q);
                rsAparams.DP = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_DP);
                rsAparams.DQ = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_DQ);
                rsAparams.InverseQ = AlignBytes(rd.ReadBytes(DecodeIntegerSize(rd)), traits.size_InvQ);

                rsa.ImportParameters(rsAparams);
                return rsa;
            }
            catch (Exception)
            {
                Debug.Assert(false);
                return null;
            }
            finally
            {
                rd.Close();
            }
        }
        #endregion
    }
}