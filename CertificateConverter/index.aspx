﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="CertificateConverter.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="semantic.min.css" />
    <style>
        .ui.input input[type="file"] {
            display: none;
        }
    </style> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <div class="ui container segment">
        <div id="DisplayError" class="ui negative message hidden" runat="server">
            <div class="header" id="ErrorHeader" runat="server"></div>
            <p id="ErrorMessage" runat="server"></p>
        </div>
        <h1 class="ui dividing header">Certificate Converter</h1>
        <form class="ui form grid segment" id="form" runat="server">
            <div class="six wide column">
                <h3 class="ui dividing header">
                  Import Certificate
                </h3>
                <div class="field">
                    <label>Old Password</label>
                    <asp:TextBox ID="OldPassword" runat="server"></asp:TextBox>
                </div>
                <div class="field">
                    <label>Certificate</label>
                    <div class="ui input">
                        <asp:TextBox ID="CertFile" runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:FileUpload ID="Cert" ClientIDMode="Static" AllowMultiple="false" onchange="this.form.submit()" runat="server" />
                   </div>
                </div>
                <h3 class="ui dividing header">
                  Export Information
                </h3>
                <div class="field">
                    <label>Friendly Name</label>
                    <asp:TextBox ID="FriendlyName" runat="server"></asp:TextBox>
                </div>
                <div class="field">
                    <label>New Password</label>
                    <asp:TextBox ID="NewPassword" runat="server"></asp:TextBox>
                </div>
                <div class="ui grid">
                    <div class="left floated ui input eight wide column">
                        <button class="ui button" type="button">Add a private key</button>
                        <asp:FileUpload ID="PrivateKeyFile" ClientIDMode="Static" AllowMultiple="false" onchange="this.form.submit()" runat="server" />
                   </div>
                    <div class="right floated ui checkbox six wide column middle aligned">
                        <asp:CheckBox ID="PrivateKey" runat="server" />
                        <label>Has private key</label>
                    </div>
                </div>
                <h3 class="ui dividing header">
                  Download Certificate
                </h3>
                <div class="three ui buttons">
                    <asp:Button CssClass="ui button" ID="CER" runat="server" Text="CER" OnClick="CER_Click" />
                    <asp:Button CssClass="ui button" ID="P12" runat="server" Text="P12" OnClick="P12_Click"/>
                    <asp:Button CssClass="ui button" ID="PFX" runat="server" Text="PFX" OnClick="PFX_Click"/>
                </div>
            </div>
            <div class="eight wide column">
                <h3 class="ui dividing header">
                  Certificate Information
                </h3>
                <div class="field">
                    <label>Subject</label>
                    <asp:TextBox ID="R_Subject" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="field">
                    <label>Issuer</label>
                    <asp:TextBox ID="R_Issuer" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="ui two column grid">
                    <div class="column field">
                        <label>Valid Date</label>
                        <asp:TextBox ID="R_ValidDate" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="column field">
                        <label>Expiry Date</label>
                        <asp:TextBox ID="R_ExpiryDate" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="field">
                    <label>Thumbprint</label>
                    <asp:TextBox ID="R_Thumbprint" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="field">
                    <label>Serial Number</label>
                    <asp:TextBox ID="R_SerialNumber" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="field">
                    <label>Public Key Format</label>
                    <asp:TextBox ID="R_PublicKeyFormat" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="ui three column grid">
                    <div class="column field">
                        <label>Version</label>
                        <asp:TextBox ID="R_Version" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="column field">
                        <label>Raw Data Length</label>
                        <asp:TextBox ID="R_RawDataLength" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="column field">
                        <label>Certificate Format</label>
                        <asp:TextBox ID="R_CertificateFormat" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="semantic.min.js"></script>
    <script type="text/javascript">
        //Add private Key
        $("button").click(function () {
            $(this).parent().find("input:file").click();
        });
        //Load Certificate
        $("input:text").click(function () {
            $(this).parent().find("input:file").click();
        });
        $('input:file', '.ui.input')
          .on('change', function (e) {
              var name = e.target.files[0].name;
              $('input:text', $(e.target).parent()).val(name);
        });
    </script>
</body>
</html>
